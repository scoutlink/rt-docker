#!/bin/bash

cron 

cat >/etc/msmtprc << EOF
defaults
logfile /var/log/msmtp.log
account default
host ${SMTP_SERVER}
port ${SMTP_PORT}
tls on
auth on
user ${SMTP_USER}
password ${SMTP_PASS}
auto_from on
EOF

fetchmail -d 15 -f /opt/rt5/etc/fetchmailrc

apache2ctl -D FOREGROUND
